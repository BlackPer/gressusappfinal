angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('welcome', {
    url: '/page1',
    templateUrl: 'templates/welcome.html',
    controller: 'welcomeCtrl'
  })

  .state('login', {
    url: '/page2',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('signup', {
    url: '/',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('tellUsMoreAboutYou', {
    url: '/page4',
    templateUrl: 'templates/tellUsMoreAboutYou.html',
    controller: 'tellUsMoreAboutYouCtrl'
  })

  .state('home', {
    url: '/page5',
    templateUrl: 'templates/home.html',
    controller: 'homeCtrl'
  })

  .state('bikeFitting', {
    url: '/page6',
    templateUrl: 'templates/bikeFitting.html',
    controller: 'bikeFittingCtrl'
  })

  .state('movementAnalysis', {
    url: '/page7',
    templateUrl: 'templates/movementAnalysis.html',
    controller: 'movementAnalysisCtrl'
  })

  .state('pacientInformation', {
    url: '/page8',
    templateUrl: 'templates/pacientInformation.html',
    controller: 'pacientInformationCtrl'
  })

  .state('grFicasMDicas', {
    url: '/page9',
    templateUrl: 'templates/grFicasMDicas.html',
    controller: 'grFicasMDicasCtrl'
  })

  .state('myDevice', {
    url: '/page10',
    templateUrl: 'templates/myDevice.html',
    controller: 'myDeviceCtrl'
  })

  .state('movikECommunity', {
    url: '/page11',
    templateUrl: 'templates/movikECommunity.html',
    controller: 'movikECommunityCtrl'
  })

  .state('directMessage', {
    url: '/page12',
    templateUrl: 'templates/directMessage.html',
    controller: 'directMessageCtrl'
  })

  .state('medicalInformation', {
    url: '/page13',
    templateUrl: 'templates/medicalInformation.html',
    controller: 'medicalInformationCtrl'
  })

  .state('newAnalysis', {
    url: '/page14',
    templateUrl: 'templates/newAnalysis.html',
    controller: 'newAnalysisCtrl'
  })

$urlRouterProvider.otherwise('/page1')


});